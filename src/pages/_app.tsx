import React from 'react';
import { AppProps } from 'next/app';
import { TodoAppProvider } from "@/app/store/AppProvider";

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
    return (
        <TodoAppProvider>
            <Component {...pageProps} />
        </TodoAppProvider>

    );
};

export default MyApp;