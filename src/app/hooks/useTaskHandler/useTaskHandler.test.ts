import { renderHook, waitFor, act } from "@testing-library/react";
import { useTaskHandler } from "../useTaskHandler/useTaskHandler";
import { createTaskListMock } from "@/app/mocks/task.mock";

describe('useTaskHandler', () => {
    const mockData = createTaskListMock(2, [
        { title: 'Task 1' },
        { title: 'Task 2' },
    ]);


    beforeEach(() => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockResolvedValue([]),
        });
    });

    it('should return the initial values', () => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);
        const { result } = renderHook(() => useTaskHandler());

        expect(result.current.tasks).toEqual([]);
        expect(result.current.onCompleteTask).toEqual(expect.any(Function));
        expect(result.current.onDeleteTask).toEqual(expect.any(Function));
    });
    it('should handle fetch success ', async() => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);

        const { result } = renderHook(() => useTaskHandler());

        await waitFor(() => {
            expect(result.current.tasks).toEqual(mockData);
        })
    });

    it('should handle fetch error', async() => {
        (global.fetch as jest.Mock).mockRejectedValue(new Error("Failed to Fetch"));
        const { result } = renderHook(() => useTaskHandler());

        expect(result.current.tasks).toEqual([]);
    });

    it('should delete a task when onDeleteTask is called with taskId', async () => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);
        const { result } = renderHook(() => useTaskHandler());

        await waitFor(() => {
            expect(result.current.tasks).toEqual(mockData);
        })

        expect(result.current.tasks.length).toEqual(2);

        act(() => {
            result.current.onDeleteTask(1);
        });
        expect(result.current.tasks.length).toEqual(1);

    });
    it('Should mark a task as completed when onCompleteTask is called with taskId', async () => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);
        const { result } = renderHook(() => useTaskHandler());

        await waitFor(() => {
            expect(result.current.tasks).toEqual(mockData);
        })

        expect(result.current.tasks[0].isCompleted).toBeFalsy();

        act(() => {
            result.current.onCompleteTask(1);
        });

        expect(result.current.tasks[0].isCompleted).toBeTruthy();

    });

    it('Should find a task when onCompleteTask is called with taskId', async () => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);
        const { result } = renderHook(() => useTaskHandler());

        await waitFor(() => {
            expect(result.current.tasks).toEqual(mockData);
        })

        let task;
        act(() => {
            task = result.current.onFindTask(2);
        });

        expect(task).toEqual(mockData[1]);

    });
    it('Should edit title task when onEditTitle is called with taskId and new title string', async () => {
        (global.fetch as jest.Mock).mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockData),
        } as any);
        const { result } = renderHook(() => useTaskHandler());

        await waitFor(() => {
            expect(result.current.tasks[1].title).toEqual(mockData[1].title);
        })

        act(() => {
            result.current.onEditTitle(2, 'Task 3');
        });

        expect(result.current.tasks[1].title).toEqual('Task 3');

    });
});