import React from 'react';
import { render, screen } from '@testing-library/react';
import { TodoAppProvider } from '../store/AppProvider';

jest.mock('../hooks/useTaskHandler/useTaskHandler', () => ({
    useTaskHandler: jest.fn().mockImplementation(() => ({
        tasks: [],
        onDeleteTask: jest.fn(),
        onCompleteTask: jest.fn(),
        onFindTask: jest.fn(),
        onEditTitle: jest.fn(),
    })),
}));
describe('TodoAppProvider', () => {
    it('Should renders children', () => {
        render(
            <TodoAppProvider>
                <div>Test Children</div>
            </TodoAppProvider>
        );
        expect(screen.getByText('Test Children')).toBeDefined();
    });
});