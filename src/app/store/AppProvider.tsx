import React, { createContext, ReactNode } from 'react';
import { useTaskHandler } from "@/app/hooks/useTaskHandler/useTaskHandler";
import { ITask } from '../interfaces/task.interface';

interface TodoAppProviderProps {
    children: ReactNode;
}
export interface TodoAppContextValue {
    tasks: ITask[];
    onDeleteTask: (id: number) => void;
    onCompleteTask: (id: number) => void;
    onFindTask: (id: number) => any;
    onEditTitle: (id: number, title: string) => void;
}

export const TodoAppContext = createContext<TodoAppContextValue>({
    tasks: [],
    onDeleteTask: () => { },
    onCompleteTask: () => { },
    onFindTask: () => { },
    onEditTitle: () => { },
});

export const TodoAppProvider = ({ children }: TodoAppProviderProps) => {
    const { tasks, onDeleteTask, onCompleteTask, onFindTask, onEditTitle } = useTaskHandler();

    return (
        <TodoAppContext.Provider value={{ tasks, onDeleteTask, onCompleteTask, onFindTask, onEditTitle }}>
            {children}
        </TodoAppContext.Provider>
    )
}