"use client"
import React, { FC, useContext } from "react";
import { ViewTask } from "../ViewTask/ViewTask";
import { TodoAppContext } from "@/app/store/AppProvider";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

export const TaskList: FC = () => {
    const { tasks } = useContext(TodoAppContext);

    return (
        <>
            {tasks.length ? (<Box sx={{ flexGrow: 1, ml: 9, mt: 5 }}>
                <Grid container spacing={2}>
                    {tasks.map((task) => (
                        <Grid key={task.id} item xs={3}>
                            <ViewTask task={task} />
                        </Grid>
                    ))}
                </Grid>
            </Box>)
                : (
                <Typography sx={{ mt: 4, ml: 9 }} variant="h4" color="text.primary">
                    Nothing to show...
                </Typography>)}
        </>
    );
};