import React from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from "@testing-library/react";
import { ViewTask } from "./ViewTask";
import { createTaskMock } from '../../mocks/task.mock';

const replace = jest.fn();
jest.mock('next/router', () => ({
  useRouter() {
    return {
      replace,
      pathname: '',
    };
  },
}));

describe('ViewTask Component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render initial task', () => {
    const mockTask = createTaskMock();
    render(<ViewTask task={mockTask} />);
    const id = screen.getByTestId(`task-id-${mockTask.id}`);
    const title = screen.getByText(`Title: ${mockTask.title}`);
    const editButton = screen.getByText('Edit');
    const incompleteIcon = screen.getByTestId('incomplete-icon');
    expect(id).toBeInTheDocument();
    expect(title).toBeInTheDocument();
    expect(editButton).toBeInTheDocument();
    expect(incompleteIcon).toBeInTheDocument();

    expect(screen.queryByTestId('complete-icon')).not.toBeInTheDocument();
  });

  it('should render initial task (complete task)', () => {
    const mockTask = createTaskMock({ isCompleted: true });
    render(<ViewTask task={mockTask} />);
    const completeIcon = screen.getByTestId('complete-icon');
    expect(completeIcon).toBeInTheDocument();

    expect(screen.queryByTestId('incomplete-icon')).not.toBeInTheDocument();
  });

  it('should click goEditTask', () => {
    const mockTask = createTaskMock();
    render(<ViewTask task={mockTask} />);
    const editButton = screen.getByText('Edit');
    fireEvent.click(editButton);
    expect(replace).toHaveBeenCalledWith(`/task/${mockTask.id}`);
  });
});