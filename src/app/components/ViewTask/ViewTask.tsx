import React from 'react';
import { FC } from "react";
import { useRouter } from 'next/router';
import { ITask } from '../../interfaces/task.interface';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AssignmentLateIcon from '@mui/icons-material/AssignmentLateOutlined';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedInOutlined';

interface ITaskProps {
    task: ITask;
}

export const ViewTask: FC<ITaskProps> = ({ task }) => {
    const router = useRouter();

    const goEditTask = () => {
        router.replace(`/task/${task.id}`)
    }

    return (
        <>
            <Card sx={{ width: 300, height: 150, mb: 3, backgroundColor: "#eeeeee" }}>
                <CardContent>
                    <Typography data-testid={`task-id-${task.id}`} sx={{ fontSize: 14 }} color="text.secondary">
                        Id: {task.id}
                    </Typography>
                    <Typography sx={{ fontSize: 17, mt: 1 }} color="text.primary" noWrap>
                        Title: {task.title}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={goEditTask}>Edit</Button>
                    {task.isCompleted ? 
                        (<AssignmentTurnedInIcon data-testid={'complete-icon'}/>) 
                    : (<AssignmentLateIcon data-testid={'incomplete-icon'}/>) }
                </CardActions>
            </Card>
        </>
    )
}