import { TodoAppContextValue } from '@/app/store/AppProvider';
import { createTaskMock } from './task.mock'

export function createAppProviderMock(data: Partial<TodoAppContextValue> = {}): TodoAppContextValue {
    return {
        tasks: [createTaskMock(), createTaskMock({id: 2}), createTaskMock({id: 3})],
        onDeleteTask: jest.fn(),
        onCompleteTask: jest.fn(),
        onFindTask: jest.fn(),
        onEditTitle: jest.fn(),
        ...data,
    };
}