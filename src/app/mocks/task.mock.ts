import { ITask } from '../interfaces/task.interface';

export const createTaskMock = (data: Partial<ITask> = {}): ITask =>{
    return {
        id: 1,
        title: 'Task 1',
        isCompleted: false,
        ...data,
    };
}

export const createTaskListMock = (taskCount = 5, overrides?: Partial<ITask>[]): ITask[] => {
    return Array.from({ length: taskCount }).map((_, index) =>
        createTaskMock({ id: index + 1, ...(overrides?.[index] || {}) })
    );
}